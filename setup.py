
# This file is part of mqtt_can4home.
#
# mqtt_can4home is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mqtt_can4home is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mqtt_can4home.  If not, see <https://www.gnu.org/licenses/>.
#
# 2019 - 2023 (c) William Chèvremont

from setuptools import setup, find_packages

setup(
    name='mqtt_can4home',
    version='0.1',
    install_requires=[
        'lightpiio',
        'typeguard',
        'paho-mqtt',
        'pyyaml',
    ],
    packages=find_packages(include=['mqtt_can4home', 'mqtt_can4home.*']),
    entry_points={'console_scripts': [
                    'mqtt_can4home=mqtt_can4home.bin.mqtt_can4home:main',
                    'canavrdude=mqtt_can4home.bin.canavrdude:main',
                    ]
                 },
)
