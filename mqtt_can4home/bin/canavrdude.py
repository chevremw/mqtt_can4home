# -*- coding: utf-8 -*-

# This file is part of mqtt_can4home.
#
# mqtt_can4home is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mqtt_can4home is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mqtt_can4home.  If not, see <https://www.gnu.org/licenses/>.
#
# 2019 - 2023 (c) William Chèvremont

from ..utils.can4home import Can4Home
import argparse
import yaml, gevent, sys

from pprint import pprint

import logging
logging.basicConfig(format="[ %(asctime)s ] %(levelname)s %(name)s : %(message)s")
logging.captureWarnings(True)

logger = logging.getLogger(__name__)

def reboot(bridge, group, node):
    bridge.clearRxBuf()    
    bridge.sendPacket(group, node, Can4Home.CommandList.DO_REBOOT)
    

def enterBootloader(bridge, group, node):
    reboot(bridge, group, node)
    while True:
        pkt = bridge.extractData( bridge.getNextCanPacket(True) )
        if pkt is None:
            continue
        logger.debug(pkt)
        
        if pkt['command'] == Can4Home.CommandList.BOOTLOADER_MODE:
            bridge.sendPacket(0x1F, 0xBB, Can4Home.CommandList.ENTER_BOOTLOADER,pkt['commandOpt'], False, pkt['rawdata'])
            break
        
    logger.debug("Enter bootloader")
    gevent.sleep(.5)
    
    bridge.sendPacket(0x1F, 0xBB, Can4Home.CommandList.BOOTLOADER_PARAMS,0,True,[],5)
    while True:
        pkt = bridge.extractData( bridge.getNextCanPacket(True) )
        if pkt is None:
            continue
        logger.debug(pkt)
       
        if pkt['command'] == Can4Home.CommandList.BOOTLOADER_PARAMS:
            logger.info(f"Bootloader version: {pkt['rawdata'][0]}.{pkt['rawdata'][1]}")
            logger.info(f"Bootloader signature: {pkt['rawdata'][2]:02x} {pkt['rawdata'][3]:02x} {pkt['rawdata'][4]:02x}")
            break
       
def read8BytesFromController(bridge, offset):
    bridge.sendPacket(0x1F, 0xBB, Can4Home.CommandList.FLASH_READ,0,False,[ offset & 0x00FF, (offset >> 8) & 0x00FF,  ])
    while True:
        pkt = bridge.extractData( bridge.getNextCanPacket(True) )
        if pkt is None:
            continue
        logger.debug(pkt)
       
        if pkt['command'] == Can4Home.CommandList.FLASH_READ:
            return pkt['rawdata']
       
def readFromController(bridge, offset, Nbytes):
    
    logger.info(f"Reading {Nbytes} bytes from controller, starting at address {offset}.")
    
    Nloop = Nbytes // 8
    
    if Nbytes % 8 > 0:
        Nloop += 1
        
    data = []
        
    for i in range(Nloop):
        data += read8BytesFromController(bridge, offset + 8*i)
        print(".",end="")
        sys.stdout.flush()
        
    print("")
        
    return data[:Nbytes]

def writeToContoller(bridge, data, pagesize):
    
    for off, d in data.items():
        
        addr = off
        datasize = len(d)
        
        logger.info(f"Writing {datasize} bytes to controller, starting at address 0x{off:04x}.")
        
        Nwrite = datasize // 8
        
        if datasize % 8 > 0:
            Nwrite += 1
            
        for i in range(Nwrite):
            bridge.sendPacket(0x1F, 0xBB, Can4Home.CommandList.FLASH_LOAD, (i*8) % pagesize, False, d[i*8:((i+1)*8)])
            
            while True: # Wait that the controller respond
                pkt = bridge.extractData( bridge.getNextCanPacket(True) )
                if pkt is None:
                    continue
                logger.debug(pkt)
               
                if pkt['command'] == Can4Home.CommandList.FLASH_LOAD:
                    break
               
            print('.', end='')
            sys.stdout.flush()
            
            
            if (i+1)*8 % pagesize == 0:
                print("")
                logger.info(f"Page loaded. Performing write at address 0x{addr:04x}")
                
                bridge.sendPacket(0x1F, 0xBB, Can4Home.CommandList.FLASH_WRITE, 0, False, [ addr & 0x00FF, (addr >> 8) & 0x00FF ,  ])
            
                while True: # Wait that the controller respond
                   pkt = bridge.extractData( bridge.getNextCanPacket(True) )
                   if pkt is None:
                       continue
                   logger.debug(pkt)
                   
                   if pkt['command'] == Can4Home.CommandList.FLASH_WRITE:
                       if not pkt['isRequest']:
                           raise RuntimeError("Response to FLASH WRITE is not a RTR...")
                       break
            
                addr = off + (i+1)*8
                logger.info("Page written successfully!")
                
        if (i+1)*8 % pagesize > 0:
            print("")
            logger.info(f"Page loaded. Performing write at address 0x{addr:04x}")
            
            bridge.sendPacket(0x1F, 0xBB, Can4Home.CommandList.FLASH_WRITE, 0, False, [ addr & 0x00FF, (addr >> 8) & 0x00FF ,  ])
        
            while True: # Wait that the controller respond
               pkt = bridge.extractData( bridge.getNextCanPacket(True) )
               if pkt is None:
                   continue
               logger.debug(pkt)
               
               if pkt['command'] == Can4Home.CommandList.FLASH_WRITE:
                    if not pkt['isRequest']:
                        raise RuntimeError("Response to FLASH WRITE is not a RTR...")
                    break
        
            addr = off + i*8
            logger.info("Page written successfully!")
            
        

def checksum(data):
    ss = sum(data)
    return (0x100 - (ss % 0x100)) & 0x00FF
        
def pageToHex(address, data, dtype=0):
    data = [len(data), (address >> 8) & 0x00FF, address & 0x00FF, dtype] + data
    
    data += [ checksum(data) ,]
    D = [f"{d:02X}" for d in data]
    return ':'+''.join(D)
    
def writeIntelFile(filename, data, linesize):
    """
    Data is dict of address : data
    """
    
    with open(filename, 'w') as fd:
        
        for a, d in data.items():
        
            N = len(d)
            Npages = N // linesize
            
            if N % linesize > 0:
                Npages += 1
            
            for i in range(Npages):
                fd.write(pageToHex(a + i*linesize, d[(i*linesize):((i+1)*linesize)])+"\n")
                
        fd.write(pageToHex(0, [], 1)+"\n")
        
def loadIntelFile(filename):
    
    rdata = dict()
    
    segment = []
    address = None
    acc = 0
    
    with open(filename, 'r') as fd:
        ll = fd.readlines()
        
        for l in ll:
            l = l[1:-1]
            
            d = [int(f"{l[2*i]}{l[2*i+1]}",16) for i in range(int(len(l)/2)) ]
            
            datalen = d[0]
            addr = (d[1] << 8) | d[2]
            dtype = d[3]
            data = d[4:-1]
            chks = d[-1]
            
            if chks != checksum(d[:-1]):
                raise RuntimeError("Checksum verification failed!")
                
            if datalen != len(data):
                raise RuntimeError("Data length do not match!")
                
            if address is None:
                address = addr
                segment += data
                acc = addr + datalen
                
            elif acc == addr:
                segment += data
                acc += datalen
            else:
                rdata[address] = segment
                address = addr
                segment = data
                acc = addr + datalen
                
            if dtype == 1:
                break
            
        else:
            raise RuntimeError("Missing end of file code.")
            
        logger.info(f"Successfully read file {filename}")
        
        return rdata
    
def checkIdentical(data, vdata):
    
    if len(data.keys()) != len(vdata.keys()):
        raise RuntimeError("EEPROM data address have different length!")
        
    for k in data:
        if k not in vdata:
            raise RuntimeError("EEPROM data address {k} mismatch")
            
        if len(data[k]) != len(vdata[k]):
            raise RuntimeError("EEPROM data length mismatch")
            
        for i in range(len(data[k])):
            if data[k] != vdata[k]:
                raise RuntimeError(f"Error at 0x{k+i:04X}: {data[k]} != {vdata[k]}")
    

def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("config", help="YAML config file")
    parser.add_argument("group", type=int, help="Group")
    parser.add_argument("node", type=int, help="Node")
    
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--upload", dest="upload_hex_file", help="Hex source code")
    group.add_argument("--check", dest="check_hex_file", help="Check hex source code")
    group.add_argument("--read", dest="read_hex_file", help="Read remote code to file")
    group.add_argument("--reboot", dest="reboot", action="store_true", default=False, help="Reboot node only")
    
    parser.add_argument("--offset", dest="offset", type=int, default=0, help="Reading address offset")
    parser.add_argument("--N", dest="N", type=int, default=8, help="Number of bytes to read")
    parser.add_argument("--pagesize", dest="pagesize", type=int, default=128, help="Number of bytes per pages")
    
    parser.add_argument('-v', dest='verbose', action='store_true', default=False, help='Verbose mode (minimum message level=debug)')                   
    parser.add_argument('--quiet', dest='quiet', action='store_true', default=False, help='Quiet mode (minimum message level=warning)')
    
    
    args = parser.parse_args()

    # Set the log level of each package according to the defined verbosity
    loglevel = logging.INFO
    
    if args.verbose:
        loglevel = logging.DEBUG
    elif args.quiet:
        loglevel = logging.WARNING
        
    for name in logging.root.manager.loggerDict:
        if name.startswith('mqtt_can4home'):
            logging.getLogger(name).setLevel(loglevel)
       
    # Get config
    cfg = None
    with open(args.config,'r') as fd:
        cfg = yaml.safe_load(fd)
       
    logger.debug(cfg)
            
    # Get Can4Home bridge connector
    bridge  = Can4Home(cfg['redisOpt'])
            
    # Reboot node only
    if args.reboot:
        reboot(bridge, args.group, args.node)
        
    elif args.read_hex_file is not None:
        enterBootloader(bridge, args.group, args.node)
        data = readFromController(bridge, args.offset, args.N)
        writeIntelFile( args.read_hex_file, { args.offset: data }, 16 )
        
        
    elif args.check_hex_file is not None:
        
        data = loadIntelFile(args.check_hex_file)                
        
    elif args.upload_hex_file is not None:
        
        data = loadIntelFile(args.upload_hex_file)
        enterBootloader(bridge, args.group, args.node)
        writeToContoller(bridge, data, args.pagesize)
        
        vdata = dict()
        
        for off, D in data.items():
            vdata[off] = readFromController(bridge, off, len(D))

        writeIntelFile(args.upload_hex_file+".verify", vdata, 16)        
        
        checkIdentical(data, vdata)
        
        logger.info(f"Check OK!")
     
            
    