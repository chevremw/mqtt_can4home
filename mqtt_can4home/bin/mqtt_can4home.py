# -*- coding: utf-8 -*-

# This file is part of mqtt_can4home.
#
# mqtt_can4home is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mqtt_can4home is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mqtt_can4home.  If not, see <https://www.gnu.org/licenses/>.
#
# 2019 - 2023 (c) William Chèvremont


import yaml
import argparse
from ..utils.can4home import Can4Home
from ..utils.rxtx_server import CanRxTxServer
from ..utils.server import Server
import signal

from pprint import pprint

import logging
logging.basicConfig(format="[ %(asctime)s ] %(levelname)s %(name)s : %(message)s")
logging.captureWarnings(True)

logger = logging.getLogger(__name__)

def readConfig(args):

    # Set the log level of each package according to the defined verbosity
    loglevel = logging.INFO
    
    if args.verbose:
        loglevel = logging.DEBUG
    elif args.quiet:
        loglevel = logging.WARNING
        
    for name in logging.root.manager.loggerDict:
        if name.startswith('mqtt_can4home'):
            logging.getLogger(name).setLevel(loglevel)

    
    cfg = None
    with open(args.config,'r') as fd:
        cfg = yaml.safe_load(fd)
        

    logger.debug(cfg)
    return cfg

def show_bus(args):
    
    cfg = readConfig(args)
    bridge  = Can4Home(cfg['redisOpt'])
    
    pprint(bridge.getBusDescription())
    
def mqtt_server(args):
    
    cfg = readConfig(args)
    mqtt_server = Server(cfg)
    
    signal.signal(signal.SIGINT, lambda *args: mqtt_server.shutdown())
    signal.signal(signal.SIGTERM, lambda *args: mqtt_server.shutdown())
    
    mqtt_server.run()
    
def bridge_server(args):
    
    cfg = readConfig(args)
    
    srv = CanRxTxServer(cfg['spibus'], cfg['intPin'],cfg['redisOpt'])
    
    signal.signal(signal.SIGINT, lambda *args: srv.stop())
    signal.signal(signal.SIGTERM, lambda *args: srv.stop())
    
    srv.serveForever(20)


def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("config", help="YAML config file")
    parser.add_argument('-v', dest='verbose', action='store_true', default=False, help='Verbose mode (minimum message level=debug)')                   
    parser.add_argument('--quiet', dest='quiet', action='store_true', default=False, help='Quiet mode (minimum message level=warning)')
    
    subparsers = parser.add_subparsers()
    command2_parser = subparsers.add_parser('show_bus')
    command2_parser.set_defaults(func=show_bus)
    
    command3_parser = subparsers.add_parser('mqtt')
    command3_parser.set_defaults(func=mqtt_server)
    
    command4_parser = subparsers.add_parser('bridge')
    command4_parser.set_defaults(func=bridge_server)
    
    args = parser.parse_args()
    

    args.func(args)
