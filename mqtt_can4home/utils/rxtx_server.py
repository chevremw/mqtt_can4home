
# This file is part of mqtt_can4home.
#
# mqtt_can4home is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mqtt_can4home is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mqtt_can4home.  If not, see <https://www.gnu.org/licenses/>.
#
# 2019 - 2023 (c) William Chèvremont


import json
import redis
import lightpiio as pio
import gevent
from .mcp2515 import MCP2515

import logging
logger = logging.getLogger(__name__)

class CanRxTxServer():
    
    def __init__(self, spidev : str, intpin : int, redisopt : dict = {'host': 'localhost', 'port': 6379}, *, oneshotmode : bool = False):
        
        self._redisdb = redis.Redis(**redisopt)
        
        self._spibus = pio.SPI(spidev, pio.SPI.Mode(0))
        self._spibus.open()
        
        self._intPin = pio.GPIO(intpin)
        self._isSent = {0: True, 1:True, 2:True}
        
        self._continue = True
        
        canspeed = MCP2515.CNF1_Flags.BRP_125KBPS
        
        self._mcp2515 = MCP2515(self._spibus)
        
        # Start config
        self._mcp2515.reset()
        
        # Config mode
        self._mcp2515.bitModify(MCP2515.RegisterMap.CANCTRL.value, MCP2515.CANCTRL_Flags.MODE_MSK, MCP2515.CANCTRL_Flags.CONFIG_MODE)  
        
        # Configuration CNF3 CNF2 CNF1
        cfg = [ 0x01, 0xD1, canspeed ]
        self._mcp2515.writeRegister(MCP2515.RegisterMap.CNF3.value, cfg)
        
        # Enable RX and TX interrupts
        self._mcp2515.writeRegister(MCP2515.RegisterMap.CANINTE.value, [int(MCP2515.CANINTF_Flags.TX2IF|MCP2515.CANINTF_Flags.TX1IF|MCP2515.CANINTF_Flags.TX0IF|MCP2515.CANINTF_Flags.RX1IF|MCP2515.CANINTF_Flags.RX0IF),])
        
        # Rollover and filters OFF
        self._mcp2515.writeRegister(MCP2515.RegisterMap.RXB0CTRL.value, [int(MCP2515.RXB0CTRL_Flags.FILTERS_OFF | MCP2515.RXB0CTRL_Flags.ROLLOVER_EN),])
        
        # GPIO
        self._interruptPin = pio.GPIO(intpin)
        self._interruptPin.export()
        self._interruptPin.setDirection(pio.GPIO.Direction.INPUT)
        self._interruptPin.setTrigger(pio.GPIO.Trigger.FALLING)
        
        # Change back to normal mode
        self._mcp2515.bitModify(MCP2515.RegisterMap.CANCTRL.value,
                                MCP2515.CANCTRL_Flags.MODE_MSK.value,
                                MCP2515.CANCTRL_Flags.ONE_SHOT_MODE.value if oneshotmode else MCP2515.CANCTRL_Flags.NORMAL_MODE.value)
        
    
    def stop(self):
        self._continue = False
    
        
    def serveForever(self, timeout):
        logger.info('Server started.')
        while self._continue:
            self._interruptPin.waitForInterrupt(timeout)
            self._processInterrupts()
            self._send(range(3))
        logger.info('Server stopped.')

    def _processInterrupts(self):
        
        while not self._interruptPin.value:
            st = self._mcp2515.status

            logger.debug(f"Status: {st}")
            
            if st & MCP2515.Status.RX0IF:
                pkt = self._mcp2515.readRxBuffer(0)
                logger.debug(f"Received [0] {pkt}")
                self._redisdb.rpush('canbus:rx', json.dumps(pkt))
            
            if st & MCP2515.Status.RX1IF:
                pkt = self._mcp2515.readRxBuffer(1)
                logger.debug(f"Received [1] {pkt}")
                self._redisdb.rpush('canbus:rx', json.dumps(pkt))
        
            if st & MCP2515.Status.TX0IF:
                self._isSent[0] = True
                self._mcp2515.bitModify(MCP2515.RegisterMap.CANINTF.value, MCP2515.CANINTF_Flags.TX0IF.value, 0x00) # Reset interrupt
        
            if st & MCP2515.Status.TX1IF:
                self._isSent[1] = True
                self._mcp2515.bitModify(MCP2515.RegisterMap.CANINTF.value, MCP2515.CANINTF_Flags.TX1IF.value, 0x00)
        
            if st & MCP2515.Status.TX2IF:
                self._isSent[2] = True
                self._mcp2515.bitModify(MCP2515.RegisterMap.CANINTF.value, MCP2515.CANINTF_Flags.TX2IF.value, 0x00)
                
    def _send(self, txbufs):
        for i in txbufs:
            if self._redisdb.llen('canbus:tx') == 0:
                return
            
            if self._isSent[i]:
                pkt = json.loads(self._redisdb.lpop('canbus:tx'))
                
                logger.debug(f"Will send {pkt}")
                
                self._mcp2515.loadTxBuffer(i, pkt)
                self._mcp2515.requestToSend(i)
        

