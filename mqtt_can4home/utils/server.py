# -*- coding: utf-8 -*-

# This file is part of mqtt_pcf8574.
#
# mqtt_pcf8574 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mqtt_pcf8574 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mqtt_pcf8574.  If not, see <https://www.gnu.org/licenses/>.
#
# 2019 - 2023 (c) William Chèvremont


from paho.mqtt import client as mqtt_client
import re
import logging
logger = logging.getLogger(__name__)
from .can4home import Can4Home
import lightpiio as pio
import time, random
import gevent, json
import sys

from pprint import pprint

class Server():
    
    def __init__(self, cfg):
        
        # Can4home protocol
        self._can4home = Can4Home(cfg['redisOpt'], self.onPkt)
        
        
        self._mqtt_cfg = cfg['mqtt']
        # mqtt topics
        self._main_topic = self._mqtt_cfg.get('main_topic', 'can4home')
        self._discovery_topic = self._mqtt_cfg.get('discovery_topic', None)
        
        # mqtt broker
        self._broker_cfg = cfg['broker']
        
        self._client = mqtt_client.Client(self._mqtt_cfg.get('client_id', f'can4home-{random.randint(0, 1000)}'))
        self._client.username_pw_set(self._broker_cfg.get('username', None), self._broker_cfg.get('password', None) )
        self._client.on_connect = self.on_connect
        self._client.on_disconnect = self.on_disconnect
        self._client.on_message = self.on_message
        
        self._discoveryUpdater = None
        
    def onPkt(self, pkt):        
        if 'dataType' in pkt:
            if pkt['dataType'] == Can4Home.DataType.CONTACT:
                val = None
                if pkt['data'] == Can4Home.ContactValue.PRESSED:
                    val = 'button_short_press'
                elif pkt['data'] == Can4Home.ContactValue.RELEASED:
                    val = "button_short_release"
                elif pkt['data'] == Can4Home.ContactValue.RELEASED_LONG:
                    val = "button_long_release"
                
                self._client.publish( f"{self._main_topic}/{pkt['groupAddress']}:{pkt['nodeAddress']}:{pkt['commandOpt']}/{val}",val , retain=False)
            
            elif pkt['dataType'] == Can4Home.DataType.SWITCH:
                self._client.publish( f"{self._main_topic}/{pkt['groupAddress']}:{pkt['nodeAddress']}:{pkt['commandOpt']}/value", 'ON' if pkt['data'] == Can4Home.SwitchValue.ON else 'OFF', retain=True)
            else:
                self._client.publish( f"{self._main_topic}/{pkt['groupAddress']}:{pkt['nodeAddress']}:{pkt['commandOpt']}/value", pkt['data'], retain=True)
        
    def connect(self):
        self._client.connect(self._broker_cfg.get('host', None), self._broker_cfg.get('port', None) )
        self._client.subscribe(f"{self._main_topic}/#")
        
    def run(self):
        
        logger.info("Starting MQTT server")
        
        self._continue = True
        
        self.connect()
        self._can4home.startTasks()
        
        if self._discoveryUpdater is None:
            self._discoveryUpdater = gevent.spawn(self._updateDiscoveryTask, 300)
        
        # self._client.loop_forever()
        
        # self._client.loop_start()
        
        while self._continue:
            self._client.loop()
            gevent.sleep(.1)

    def shutdown(self):
        # Send the unavailable state for all channels

        logger.info("Shutting down MQTT server")
        
        self._continue = False
        
        self._can4home.stopTasks()
        
        chdesc = self._can4home.getBusDescription()
        # Update availability topics
        for g, G in chdesc.items():
            for n, N in G.items():
                for c, C in N['channels'].items():
                    if C['type'] != Can4Home.DataType.CONTACT: # Contact has no availability channel
                        self._client.publish(f"{self._main_topic}/{g}:{n}:{c}/available", "OFFLINE", retain=True)    

        self._client.disconnect()
        # self._client.loop_stop()
        
        # if self._discoveryUpdater is not None:
            # self._discoveryUpdater.kill()
            # self._discoveryUpdater = None
        
    def _updateDiscoveryTask(self, timeout):
        while self._continue:
            chdesc = self._can4home.getBusDescription()
            self._updateDiscovery(chdesc)
            gevent.sleep(timeout)
        
    def _updateDiscovery(self, chdesc):
        if self._discovery_topic is not None: # Set the discovery parameters if discovery is activated
            
            for g, G in chdesc.items():
                for n, N in G.items():
                    for c, C in N['channels'].items():
                        
                        if c == 0:
                            continue
                            
                        dpayload = dict()
                            
                        dpayload['availability'] = dict()
                        dpayload['availability']['topic'] = f"{self._main_topic}/{g}:{n}:{c}/available"
                        dpayload['availability']['payload_available'] = "ONLINE"
                        dpayload['availability']['payload_not_available'] = "OFFLINE"
                            
                        dpayload['device'] = dict()
                        dpayload['device']['identifiers'] = f"{g}:{n}:{c}"
                        if N['serial'] is not None:
                            dpayload['device']['connections'] = [('mac', N['serial'])]
                        dpayload['device']['manufacturer'] = "Can4Home"
                        dpayload['device']['name'] = f"Node {g}:{n}"
                        
                        dpayload['name'] = f"Channel {c}"
                            
                        dpayload['qos'] = 0
                        
                        if C['type'] in ( Can4Home.DataType.UINT8,
                                          Can4Home.DataType.UINT16,
                                          Can4Home.DataType.UINT32,
                                          Can4Home.DataType.UINT64,
                                          Can4Home.DataType.INT8,
                                          Can4Home.DataType.INT16,
                                          Can4Home.DataType.INT32,
                                          Can4Home.DataType.INT64, ):
                            
                            dpayload['state_class'] = 'measurement'
                            
                            if C['type'] in (Can4Home.DataType.FLOAT32, Can4Home.DataType.FLOAT64):
                                dpayload['value_template'] = "{{ value | float  | round (3) }}"
                            else:
                                dpayload['value_template'] = "{{ value | int }}"
                                
                            if C['read']:
                                dpayload['state_topic'] = f"{self._main_topic}/{g}:{n}:{c}/value"
                            
                            if C['write']: # This is a number
                                
                                dpayload['command_topic'] = f"{self._main_topic}/{g}:{n}:{c}/set"

                                if C['type'] == Can4Home.DataType.UINT8:
                                    dpayload['min'] = 0
                                    dpayload['max'] = 2**8-1
                                elif C['type'] == Can4Home.DataType.UINT16:
                                    dpayload['min'] = 0
                                    dpayload['max'] = 2**16-1
                                elif C['type'] == Can4Home.DataType.UINT32:
                                    dpayload['min'] = 0
                                    dpayload['max'] = 2**32-1
                                elif C['type'] == Can4Home.DataType.UINT64:
                                    dpayload['min'] = 0
                                    dpayload['max'] = 2**64-1
                                elif C['type'] == Can4Home.DataType.INT8:
                                    dpayload['min'] = -2**7
                                    dpayload['max'] = 2**7-1
                                elif C['type'] == Can4Home.DataType.INT16:
                                    dpayload['min'] = -2**15
                                    dpayload['max'] = 2**15-1
                                elif C['type'] == Can4Home.DataType.INT32:
                                    dpayload['min'] = -2**31
                                    dpayload['max'] = 2**31-1
                                elif C['type'] == Can4Home.DataType.INT64:
                                    dpayload['min'] = -2**63
                                    dpayload['max'] = 2**63-1

                                
                                dpayload['optimistic'] = not C['read']
                                dpayload['step'] = 1
                                dpayload['unique_id'] = f"{g}:{n}:{c}_number"
                                
                               
                                
                                self._client.publish(f"{self._discovery_topic}/number/{g}_{n}/{c}/config", json.dumps(dpayload), retain=True)
                                    
                            else: # This is a sensor (read only)
                                dpayload['unique_id'] = f"{g}:{n}:{c}_sensor"
                                
                                self._client.publish(f"{self._discovery_topic}/sensor/{g}_{n}/{c}/config", json.dumps(dpayload), retain=True)
                            
                        
                        elif C['type'] in ( Can4Home.DataType.FLOAT32,
                                            Can4Home.DataType.FLOAT64 ):
                                
                            if C['read']:
                                dpayload['state_topic'] = f"{self._main_topic}/{g}:{n}:{c}/value"
                            
                            if C['write']: # This is a number
                                
                                dpayload['command_topic'] = f"{self._main_topic}/{g}:{n}:{c}/set"

                                dpayload['min'] = sys.float_info.min
                                dpayload['max'] = sys.float_info.max
                                
                                dpayload['optimistic'] = not C['read']
                                dpayload['step'] = .000001
                                dpayload['unique_id'] = f"{g}:{n}:{c}_number"
                                
                                self._client.publish(f"{self._discovery_topic}/number/{g}_{n}/{c}/config", json.dumps(dpayload), retain=True)
                                    
                            else: # This is a sensor (read only)
                                dpayload['unique_id'] = f"{g}:{n}:{c}_sensor"
                                
                                self._client.publish(f"{self._discovery_topic}/sensor/{g}_{n}/{c}/config", json.dumps(dpayload), retain=True)
                            
                        elif C['type'] == Can4Home.DataType.SWITCH:
            
                            if C['read']:
                                dpayload['state_topic'] = f"{self._main_topic}/{g}:{n}:{c}/value"
                                
                                dpayload['state_on'] = 'ON'
                                dpayload['state_off'] = 'OFF'
                                
                            dpayload['command_topic'] = f"{self._main_topic}/{g}:{n}:{c}/set"
            
                            dpayload['payload_on'] = 'ON'
                            dpayload['payload_off'] = 'OFF'
                            
                            dpayload['optimistic'] = not C['read']
                                
                            dpayload['unique_id'] = f"{g}:{n}:{c}_switch"
                            self._client.publish(f"{self._discovery_topic}/switch/{g}_{n}/{c}/config", json.dumps(dpayload), retain=True)
                            
                        
                        elif C['type'] == Can4Home.DataType.CONTACT:
                            
                            del dpayload['availability']
                            
                            for trigger_type in ('button_short_press','button_short_release','button_long_release'):
                                dpayload['device']['identifiers'] = f"{g}:{n}:{c}_{trigger_type}"
                                dpayload['type'] = trigger_type
                                dpayload['subtype'] = f'button_{c}'
                                dpayload['topic'] = f"{self._main_topic}/{g}:{n}:{c}/{trigger_type}"
                                dpayload['automation_type'] = "trigger"
                                self._client.publish(f"{self._discovery_topic}/device_automation/{g}_{n}_{c}_{trigger_type}/config", json.dumps(dpayload), retain=True)
                        
                        elif C['type'] == Can4Home.DataType.COLOR:
                            
                            if C['read']:
                                dpayload['state_topic'] = f"{self._main_topic}/{g}:{n}:{c}/value"
                                dpayload['rgbw_state_topic'] = f"{self._main_topic}/{g}:{n}:{c}/color"
                                
                                dpayload['state_on'] = 'ON'
                                dpayload['state_off'] = 'OFF'
                                
                            dpayload['command_topic'] = f"{self._main_topic}/{g}:{n}:{c}/set"
                            dpayload['rgbw_command_topic'] = f"{self._main_topic}/{g}:{n}:{c}/setcolor"
            
                            dpayload['payload_on'] = 'ON'
                            dpayload['payload_off'] = 'OFF'
                            
                            dpayload['optimistic'] = not C['read']
                                
                            dpayload['unique_id'] = f"{g}:{n}:{c}_light_rgbw"
                            self._client.publish(f"{self._discovery_topic}/light/{g}_{n}/{c}/config", json.dumps(dpayload), retain=True)
                        
                        elif C['type'] == Can4Home.DataType.DIMMER:
                            
                            if C['read']:
                                dpayload['state_topic'] = f"{self._main_topic}/{g}:{n}:{c}/value"
                                dpayload['brightness_state_topic'] = f"{self._main_topic}/{g}:{n}:{c}/brightness"
                                
                                dpayload['state_on'] = 'ON'
                                dpayload['state_off'] = 'OFF'
                                
                            dpayload['command_topic'] = f"{self._main_topic}/{g}:{n}:{c}/set"
                            dpayload['brightness_command_topic'] = f"{self._main_topic}/{g}:{n}:{c}/setbrightness"
            
                            dpayload['payload_on'] = 'ON'
                            dpayload['payload_off'] = 'OFF'
                            
                            dpayload['optimistic'] = not C['read']
                                
                            dpayload['unique_id'] = f"{g}:{n}:{c}_light_brightness"
                            self._client.publish(f"{self._discovery_topic}/light/{g}_{n}/{c}/config", json.dumps(dpayload), retain=True)
                        
                        elif C['type'] == Can4Home.DataType.TEMPERATURE:
                            dpayload['unique_id'] = f"{g}:{n}:{c}_temperature"
                            dpayload['unit_of_measurement'] = "°C"
                            dpayload['device_class'] = "temperature"
                            dpayload['state_class'] = 'measurement'
                            
                            if C['read']:
                                dpayload['state_topic'] = f"{self._main_topic}/{g}:{n}:{c}/value"
                            
                            if C['write']: # This is a number
                                
                                dpayload['command_topic'] = f"{self._main_topic}/{g}:{n}:{c}/set"

                                dpayload['min'] = -273.15
                                dpayload['max'] = 350
                                
                                dpayload['optimistic'] = not C['read']
                                dpayload['step'] = .1
                                
                                self._client.publish(f"{self._discovery_topic}/number/{g}_{n}/{c}/config", json.dumps(dpayload), retain=True)
                                    
                            else: # This is a sensor (read only)
                                self._client.publish(f"{self._discovery_topic}/sensor/{g}_{n}/{c}/config", json.dumps(dpayload), retain=True)
                        
                        elif C['type'] == Can4Home.DataType.HUMIDITY:
                            dpayload['unique_id'] = f"{g}:{n}:{c}_humidity"
                            dpayload['unit_of_measurement'] = "%"
                            dpayload['device_class'] = "humidity"
                            dpayload['state_class'] = 'measurement'
                            
                            if C['read']:
                                dpayload['state_topic'] = f"{self._main_topic}/{g}:{n}:{c}/value"
                            
                            if C['write']: # This is a number
                                
                                dpayload['command_topic'] = f"{self._main_topic}/{g}:{n}:{c}/set"

                                dpayload['min'] = 0
                                dpayload['max'] = 100
                                
                                dpayload['optimistic'] = not C['read']
                                dpayload['step'] = .1
                                
                                self._client.publish(f"{self._discovery_topic}/number/{g}_{n}/{c}/config", json.dumps(dpayload), retain=True)
                                    
                            else: # This is a sensor (read only)
                                self._client.publish(f"{self._discovery_topic}/sensor/{g}_{n}/{c}/config", json.dumps(dpayload), retain=True)
                        
                        elif C['type'] == Can4Home.DataType.ROLLER:
                            pass
                        
                        else:
                            logger.warning(f"Unknow channel type {C['type']}")
        

    def on_connect(self, client, userdata, flags, rc ):
        if rc == 0:
            logger.info("Connected to MQTT broker")
            
            chdesc = self._can4home.getBusDescription()
            
            self._updateDiscovery(chdesc)
                                
            # Update availability topics
            for g, G in chdesc.items():
                for n, N in G.items():
                    for c, C in N['channels'].items():
                        
                        if C['type'] != Can4Home.DataType.CONTACT: # Contact has no availability channel
                            self._client.publish(f"{self._main_topic}/{g}:{n}:{c}/available", "ONLINE", retain=True)            
                                             
        else:
            logger.error(f"Failed to connect to MQTT broker ({rc})")
            
    
    def on_disconnect(self, client, userdata, rc):

        if rc == 0: # The disconnection come from disconnect() method, do not try to reconnect
            return

        FIRST_RECONNECT_DELAY = 1
        RECONNECT_RATE = 2
        MAX_RECONNECT_COUNT = 12
        MAX_RECONNECT_DELAY = 60
    
        logger.info("Disconnected with result code: %s", rc)
        reconnect_count, reconnect_delay = 0, FIRST_RECONNECT_DELAY
        while reconnect_count < MAX_RECONNECT_COUNT:
            logger.info("Reconnecting in %d seconds...", reconnect_delay)
            time.sleep(reconnect_delay)
    
            try:
                client.reconnect()
                logging.info("Reconnected successfully!")
                return
            except Exception as err:
                logging.error("%s. Reconnect failed. Retrying...", err)
    
            reconnect_delay *= RECONNECT_RATE
            reconnect_delay = min(reconnect_delay, MAX_RECONNECT_DELAY)
            reconnect_count += 1
        logger.info("Reconnect failed after %s attempts. Exiting...", reconnect_count)

            
    def on_message(self, client, userdata, msg):
        
        what = msg.topic.split('/')
        
        if what[-1] == 'set': # Write to controller
            
            addr = what[-2].split(':')
            cht = self._can4home.getChannelType(*addr)
            pl = msg.payload.decode()
            
            logger.debug(f"Writing {pl} to {addr}, which is {cht}")
            
            if cht is None:
                logger.warning(f"Try to write to unknow channel {addr} {msg.payload}")
                return
            
            elif cht == Can4Home.DataType.SWITCH:
                if pl == 'ON':
                    self._can4home.writeToChannel(*[int(a) for a in addr], [ Can4Home.SwitchValue.ON.value,])
                elif pl == 'OFF':
                    self._can4home.writeToChannel(*[int(a) for a in addr], [ Can4Home.SwitchValue.OFF.value,])
                else:
                    logger.error(f"Unknow payload {pl} for type {cht}")
                
            else:
                logger.warning(f"Unsupported type {cht}")
            
            
        
            
