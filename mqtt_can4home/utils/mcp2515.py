# -*- coding: utf-8 -*-

# This file is part of mqtt_can4home.
#
# mqtt_can4home is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mqtt_can4home is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mqtt_can4home.  If not, see <https://www.gnu.org/licenses/>.
#
# 2019 - 2023 (c) William Chèvremont


import lightpiio as pio
from typeguard import typechecked
import enum
from collections.abc import Sequence
import gevent
import logging
logger = logging.getLogger(__name__)

class MCP2515:
    
    class Commands(enum.Enum):
        WRITEREG            = 0x02
        READREG             = 0x03
        BITMODIFY           = 0x05
        LOADTX              = 0x40
        RTS                 = 0x80
        READRX              = 0x90
        READST              = 0xa0
        READRXST            = 0xb0
        RESET               = 0xc0
        
    class RegisterMap(enum.Enum):
        CANCTRL             = 0x0F
        CNF3                = 0x28
        CNF2                = 0x29
        CNF1                = 0x2A
        CANINTE             = 0x2B
        CANINTF             = 0x2C
        RXB0CTRL            = 0x60
        
    class CANCTRL_Flags(enum.IntFlag):
        NORMAL_MODE         = 0x00
        CLOCK_DIV_2         = 0x01
        CLOCK_DIV_4         = 0x02
        CLOCK_DIV_8         = 0x03
        CLOCK_EN            = 0x04
        ONE_SHOT_MODE       = 0x08
        REQUEST_ABORT       = 0x10
        SLEEP_MODE          = 0x20
        LOOPBACK_MODE       = 0x40
        LISTEN_MODE         = 0x60
        CONFIG_MODE         = 0x80
        
        MODE_MSK            = 0xE0
        ABORT_MSK           = 0x10
        OSM_MSK             = 0x80
        CLK_MSK             = 0x04
        DIV_MSK             = 0x03
        
    class CNF1_Flags(enum.IntFlag):
        SYNC_JUMP_WIDTH_1   = 0x00
        SYNC_JUMP_WIDTH_2   = 0x40
        SYNC_JUMP_WIDTH_3   = 0x80
        SYNC_JUMP_WIDTH_4   = 0xC0
        BRP_500KBPS         = 0x00
        BRP_250KBPS         = 0x01
        BRP_125KBPS         = 0x03
        BRP_62p5KBPS        = 0x07
        
    class CNF2_Flags(enum.IntFlag):
        BTLMODE             = 0x80
        SAMPLE_3            = 0x40
        
        PHSEG_MSK           = 0x38
        PRSEG_MSK           = 0x07
        
    class CNF3_Flags(enum.IntFlag):
        SOF_CLK             = 0x80
        WUP_FLT             = 0x40
        PHSEG2_MSK          = 0x07
        
    class CANINTF_Flags(enum.IntFlag):
        MERRF               = 0x80
        WAKIF               = 0x40
        ERRIF               = 0x20
        TX2IF               = 0x10
        TX1IF               = 0x08
        TX0IF               = 0x04
        RX1IF               = 0x02
        RX0IF               = 0x01
        
    class Status(enum.IntFlag):
        RX0IF               = 0x01
        RX1IF               = 0x02
        TX1REQ              = 0x04
        TX0IF               = 0x08
        TX0REQ              = 0x10
        TX1IF               = 0x20
        TX2REQ              = 0x40
        TX2IF               = 0x80
        
    class RXStatus(enum.IntFlag):
        RXF0                = 0x00
        RXF1                = 0x01
        RXF2                = 0x02
        RXF3                = 0x03
        RXF4                = 0x04
        RXF5                = 0x05
        DATA_FRAME          = 0x00
        REMOTE_FRAME        = 0x08
        STANDARD_FRAME      = 0x00
        EXTENDED_FRAME      = 0x10
        RXB0                = 0x40
        RXB1                = 0x80
        
    class RXB0CTRL_Flags(enum.IntFlag):
        FILTERS_OFF         = 0x60
        ROLLOVER_EN         = 0x04
        
        
    
#    @typechecked
    def __init__(self, spidev : pio.SPI):
        self._spidev = spidev
            
    @typechecked
    def readRxBuffer(self, bufferid : int):
        
        logger.debug(f"Read RX buffer {bufferid}")

        if bufferid < 0 or bufferid > 1:
            raise ValueError(f"Invalid buffer id {bufferid}")
            
        txb = [ self.Commands.READRX.value + (bufferid << 2) ,] + [0,]*13
        tmp = self._spidev.transfer(txb)
        
        logger.debug(f"After transfer {tmp}")

        return tmp[1:]
    
    def loadTxBuffer(self, bufferid : int, msg : Sequence[int]):
        
        logger.debug(f"load TX {bufferid}")

        if bufferid not in (0,1,2):
            raise ValueError(f"Invalid buffer id {bufferid}")
            
        if len(msg) != 13:
            raise ValueError("Message must have 13 elements")
            
        self._spidev.transfer([ self.Commands.LOADTX.value | (bufferid << 1) ] + msg)
        
        logger.debug(f"After transfer")
    
    def requestToSend(self, bufferid : int):
        
        logger.debug(f"RTS {bufferid}")
    
        if bufferid not in (0,1,2):
            raise ValueError(f"Invalid buffer id {bufferid}")
            
        self._spidev.transfer([ self.Commands.RTS.value | (1 << bufferid), ])
        
        logger.debug("After transfer")
    
    
    def reset(self):
        self._spidev.transfer([self.Commands.RESET.value])
        gevent.sleep(2)
        
    @property
    def status(self):
        return self.Status(self._spidev.transfer([self.Commands.READST.value, 0x00])[1])
        
    @property
    def rxStatus(self):
        return self.RXStatus(self._spidev.transfer([self.Commands.READRXST.value, 0x00])[1])
        
    @typechecked
    def bitModify(self, address : int, mask : int, value : int):
        request = [ self.Commands.BITMODIFY.value, address, mask, value ]
        self._spidev.transfer(request)
        
    @typechecked
    def writeRegister(self, address : int, values : Sequence[int]):
        request = [ self.Commands.WRITEREG.value, address, ] + [ v for v in values ]
        self._spidev.transfer(request)
        
    @typechecked
    def readRegister(self, address : int, nbytes : int):
        request = [self.Commands.READREG.value, address, ] + [0,]*nbytes
        return self._spidev.transfer(request)[2:]

