# -*- coding: utf-8 -*-

# This file is part of mqtt_can4home.
#
# mqtt_can4home is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mqtt_can4home is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mqtt_can4home.  If not, see <https://www.gnu.org/licenses/>.
#
# 2019 - 2023 (c) William Chèvremont


import enum
import struct
from typeguard import typechecked
from typing import Optional
from collections.abc import Sequence, Mapping, Callable
import gevent
import redis
import json

import logging
logger = logging.getLogger(__name__)

import pprint


class Can4Home():
    
    class CommandList(enum.Enum):
        DATA                = 0x00
        WRITE_DATA          = 0x01
        CHAN_DESCR          = 0x02
        ACTIVATE_EVENT      = 0x03
        MSG_DEBUG           = 0x70
        MSG_INFO            = 0x71
        MSG_WARNING         = 0x72
        MSG_ERROR           = 0x73
        DO_REBOOT           = 0x80
        BOOTLOADER_MODE     = 0x81
        ENTER_BOOTLOADER    = 0x82
        NORMAL_MODE         = 0x83
        PINGPONG            = 0x90
        SERIALNO            = 0x91
        FIRMWARE_VER        = 0x92
        DEVICE_SIGNATURE    = 0x93
        NEWADDRESS          = 0x94
        BOOTLOADER_PARAMS   = 0xB0
        FLASH_LOAD          = 0xB1
        FLASH_WRITE         = 0xB2
        FLASH_READ          = 0xB3
        
    class DataType(enum.Enum):
        UINT8               = 0x01
        UINT16              = 0x02
        UINT32              = 0x03
        UINT64              = 0x04
        INT8                = 0x05
        INT16               = 0x06
        INT32               = 0x07
        INT64               = 0x08
        FLOAT32             = 0x09
        FLOAT64             = 0x0A
        SWITCH              = 0x10
        CONTACT             = 0x11
        COLOR               = 0x12
        DIMMER              = 0x13
        TEMPERATURE         = 0x14
        HUMIDITY            = 0x15
        ROLLER              = 0x16      
        
    class SwitchValue(enum.Enum):
        OFF                 = 0x00
        ON                  = 0x01
        
    class ContactValue(enum.Enum):
        PRESSED             = 0x01
        RELEASED            = 0x02
        RELEASED_LONG       = 0x03
        
    class RollerState(enum.IntFlag):
        TOP_LIMIT           = 0x01
        BOTTOM_LIMIT        = 0x02
        MOVING              = 0x04
        MOVING_UP           = 0x08
        ERROR               = 0x10
    
    @typechecked
    def __init__(self, redisOpt : Mapping, callback : Optional[Callable] = None):
        
        self._redisdb = redis.Redis(**redisOpt)
        
        self._pktCallback = callback
        self._continue = True
        
        #self._channelMap = dict() # Group then Node then Channel ID
        #self._busMap = dict()
        
        self._typesLength = dict()
        self._typesLength[self.DataType.INT8] = 1
        self._typesLength[self.DataType.INT16] = 2
        self._typesLength[self.DataType.INT32] = 4
        self._typesLength[self.DataType.INT64] = 8
        self._typesLength[self.DataType.UINT8] = 1
        self._typesLength[self.DataType.UINT16] = 2
        self._typesLength[self.DataType.UINT32] = 4
        self._typesLength[self.DataType.UINT64] = 8
        
        self._typesLength[self.DataType.FLOAT32] = 4
        self._typesLength[self.DataType.FLOAT64] = 8
        
        self._typesLength[self.DataType.SWITCH] = 1
        self._typesLength[self.DataType.CONTACT] = 1
        self._typesLength[self.DataType.COLOR] = 4
        self._typesLength[self.DataType.DIMMER] = 1
        self._typesLength[self.DataType.TEMPERATURE] = 2
        self._typesLength[self.DataType.HUMIDITY] = 1
        self._typesLength[self.DataType.ROLLER] = 2
        
        self.__periodicCheckTask = None
        self.__readingTask = None
        
    def stopTasks(self):
        
        logger.info("Stopping can4home tasks")
        self._continue = False
        # if self.__readingTask is not None:
        #     self.__readingTask.join()
        #     self.__readingTask = None
        
        # if self.__periodicCheckTask is not None:
        #     self.__periodicCheckTask.join()
        #     self.__periodicCheckTask = None
                
    def startTasks(self):      
        
        logger.info("Starting can4home tasks")
        
        self._continue = True
        
        if self.__readingTask is None:
            self.__readingTask = gevent.spawn(self._readingTask)
            
        if self.__periodicCheckTask is None:
            self.__periodicCheckTask = gevent.spawn(self._periodicCheckTask, 120)

    def _readingTask(self):

        while self._continue:
            # logger.debug("Can4Home read from redis")
            while self._continue and self._redisdb.llen('canbus:rx') > 0:
                canpkt = self.getNextCanPacket()
                c4hpkt = self.fromCan(canpkt)
                
                logger.debug(f"Received a new CAN packet. Raw: {canpkt} Processed: {c4hpkt}")
                
                if self._pktCallback is not None and c4hpkt is not None:
                    self._pktCallback(c4hpkt)
                
            gevent.sleep(.1)
        
    def getNextCanPacket(self, wait=False):
        pkt = None
        if wait:
            while pkt is None:
                pkt = self._redisdb.lpop('canbus:rx')
                gevent.sleep(.01)
            return json.loads(pkt)
        
        return json.loads(self._redisdb.lpop('canbus:rx'))
        
    @typechecked
    def sendPacket(self, grpAddress : int, nodeAddress : int, command : 'Can4Home.CommandList', commandOpt : int = 0, isRequest : bool = False, data : Sequence[int] = [], dataLength : int = 0):

        msgpkt = self.toCan(grpAddress, nodeAddress, command, commandOpt, isRequest, data, dataLength)        

        if len(msgpkt) != 13:
            raise RuntimeError("CAN packet must be 13 byte array")
            
        self._redisdb.rpush('canbus:tx', json.dumps(msgpkt))
        
    @typechecked
    def toCan(self, grpAddress : int, nodeAddress : int, command : 'Can4Home.CommandList', commandOpt : int = 0, isRequest : bool = False, data : Sequence[int] = [], dataLength : int = 0):
        
        if grpAddress < 0 or grpAddress > 31:
            raise ValueError("Group address must be in range 0-31")
            
        if nodeAddress < 0 or nodeAddress > 255:
            raise ValueError("Node address must be in range 0-255")
            
        if commandOpt < 0 or commandOpt > 255:
            raise ValueError("CommandOpt must be in range 0-255")
            
        if len(data) > 8:
            raise ValueError("Data must have at most 8 values")
            
        if dataLength < 0 or dataLength > 8:
            raise ValueError("Request length must be in range 0-8")
            
        for d in data:
            if d < 0 or d > 255:
                raise ValueError("Data must be in range 0-255")
                
        if isRequest and len(data) > 0:
            raise ValueError("Can't be request and have data!")
            
        if not isRequest:
            dataLength = len(data)
            
        while len(data) < 8:
            data += [0,]
            
        # Build message
        msgpkt = [ nodeAddress, 
                  ((grpAddress & 0x1C) << 3) | (grpAddress & 0x03) | 0x08,
                  command.value,
                  commandOpt,
                  dataLength | 0x40 if isRequest else dataLength
                  ] + data
        
        logger.debug(msgpkt)
        
        if len(msgpkt) != 13:
            raise RuntimeError(f"Error while builing the message. {msgpkt}")
            
        return msgpkt
    
    @typechecked
    def extractData(self, msgpkt : Sequence[int]):
        try:
            r = {}
            r['nodeAddress'] = msgpkt[0]
            r['groupAddress'] = ((msgpkt[1] & 0xE0) >> 3 | (msgpkt[1] & 0x3))
            r['command'] = self.CommandList(msgpkt[2])
            r['commandOpt'] = msgpkt[3]
            r['isRequest'] = (msgpkt[4] & 0x40) > 0
            dl = msgpkt[4] & 0x0F
            r['rawdata'] = msgpkt[5:dl+5] if not r['isRequest'] else None
            r['dataLength'] = dl
            
            return r
        except Exception as e:
            logger.error(str(e))
            return None
    
    def clearRxBuf(self):
        self._redisdb.ltrim('canbus:rx', 0,-1)
    
    @typechecked
    def fromCan(self, msgpkt : Sequence[int]):

        if len(msgpkt) != 13:
            raise ValueError("CAN message must be a 13 bytes array")
            
        r = self.extractData(msgpkt)
        r['data'] = None
        
        if r['groupAddress'] == 0x1F: # This is reserved group: ignore.
            return None
        
        # Declare the node in redis if needed
        self._redisdb.sadd('can4home:groups', str(r['groupAddress']))
        self._redisdb.sadd(f"can4home:group_{r['groupAddress']}:nodes", str(r['nodeAddress']))
        
        # Process the packet. Return None if this is an internal packet, return the processed packet
        # if this is for user purpose.
        
        if r['command'] == self.CommandList.DATA: # Data received
            
            if r['commandOpt'] == 0: # This is number of channel
                    
                logger.debug(f"Got number of channels {r['groupAddress']}:node_{r['nodeAddress']} {r['rawdata'][0]}")
            
                self._redisdb.set(f"can4home:group_{r['groupAddress']}:node_{r['nodeAddress']}:nb_channels", r['rawdata'][0])
                
                return None                    
                
            else:
                chantype = self._redisdb.get(f"can4home:group_{r['groupAddress']}:node_{r['nodeAddress']}:channel_{r['commandOpt']}:type")
                if chantype is not None:
                    
                    chantype = self.DataType(int(chantype.decode()))
                    
                    r['channelId'] = r['commandOpt']
                    r['dataType'] = chantype
                    dl = r['dataLength']
                    
                    if dl == self._typesLength[chantype]:
                    
                        if chantype in (self.DataType.INT8, self.DataType.UINT8,
                                    self.DataType.INT16, self.DataType.UINT16,
                                    self.DataType.INT32, self.DataType.UINT32,
                                    self.DataType.INT64, self.DataType.UINT64,):
                            r['data'] = int.from_bytes(r['rawdata'], 'big', signed= chantype in (self.DataType.INT8,
                                                                                                 self.DataType.INT16,
                                                                                                 self.DataType.INT32,
                                                                                                 self.DataType.INT64,))
                            
                        elif chantype == self.DataType.FLOAT32:
                            r['data'] = struct.unpack('f', r['rawdata'])
                            
                        elif chantype == self.DataType.FLOAT64:
                            r['data'] = struct.unpack('d', r['rawdata'])
                            
                        elif chantype == self.DataType.SWITCH:
                            r['data'] = self.SwitchValue(r['rawdata'][0])
                            
                        elif chantype == self.DataType.CONTACT:
                            r['data'] = self.ContactValue(r['rawdata'][0])
                            
                        elif chantype == self.DataType.COLOR:
                            r['data'] = {'intensity': r['rawdata'][0],
                                         'red': r['rawdata'][1],
                                         'green': r['rawdata'][2],
                                         'blue': r['rawdata'][3],
                                         }
                            
                        elif chantype in (self.DataType.DIMMER, self.DataType.HUMIDITY, ):
                            r['data'] = r['rawdata'][0]/255.
                            
                        elif chantype == self.DataType.TEMPERATURE:
                            v = (r['rawdata'][0] << 8) + r['rawdata'][1]
                            r['data'] = (v - 27315)/100.
                            
                        elif chantype == self.DataType.ROLLER:
                            r['data'] = (self.RollerState(r['rawdata'][0]), r['rawdata'][1]/255.)
                            
                    else:
                        logger.error("Mismatch between data length and channel type!")
                else:
                    logger.warning("Channel is not known in channel map!")
                
            logger.debug(f"DATA packet: {r}")
                    
            return r
        
        elif r['command'] == self.CommandList.CHAN_DESCR: # Fill channel map
            # if r['groupAddress'] not in self._channelMap:
            #     self._channelMap[r['groupAddress']] = dict()
                
            # if r['nodeAddress'] not in self._channelMap:
            #     self._channelMap[r['groupAddress']][r['nodeAddress']] = dict()
                
            # self._channelMap[r['groupAddress']][r['nodeAddress']][r['commandOpt']] = ( self.channelType(r['rawdata'][0] & 0x3F),
            #                                                                           r['rawdata'] & 0x40 > 0, # WRITE
            #                                                                           r['rawdata'] & 0x80 > 0, # READ
            #                                                                           )
            
            self._redisdb.set(f"can4home:group_{r['groupAddress']}:node_{r['nodeAddress']}:channel_{r['commandOpt']}:type", r['rawdata'][0] & 0x3F)
            self._redisdb.set(f"can4home:group_{r['groupAddress']}:node_{r['nodeAddress']}:channel_{r['commandOpt']}:write", 'true' if r['rawdata'][0] & 0x40 > 0 else 'false')
            self._redisdb.set(f"can4home:group_{r['groupAddress']}:node_{r['nodeAddress']}:channel_{r['commandOpt']}:read",  'true' if r['rawdata'][0] & 0x80 > 0 else 'false')
            
            
        elif r['command'] == self.CommandList.SERIALNO:
            # if r['groupAddress'] not in self._busMap:
            #     self._busMap[r['groupAddress']] = dict()
                
            serialno = [ r['commandOpt'], ] + r['rawdata']
            serialno = ':'.join([f"{v:02x}" for v in serialno])
                
            # self._busMap[r['groupAddress']][r['nodeAddress']] = serialno
            
            self._redisdb.set(f"can4home:group_{r['groupAddress']}:node_{r['nodeAddress']}:serial", serialno)
            
            logger.info(f"Node serial number found: {r['groupAddress']} :: {r['nodeAddress']} {serialno}")
            
        else:
            logger.warning(f"Unprocessed CAN packet: {r}")
        
        return None
        
    def clearNodeData(self, group: int, node: int):
        """
        Clear data from a node. this will trigger a rescan of the node by the periodic check
        """
        
        logger.debug(f"Clearing node {group}::{node}")
        
        self._redisdb.sadd('can4home:groups', str(group))
        self._redisdb.sadd(f"can4home:group_{group}:nodes", str(node))
        
        self._redisdb.delete(f"can4home:group_{group}:node_{node}:serial")
        self._redisdb.delete(f"can4home:group_{group}:node_{node}:nb_channels")
        
        for chid in range(256):
            self._redisdb.delete(f"can4home:group_{group}:node_{node}:channel_{chid}:type",
                                 f"can4home:group_{group}:node_{node}:channel_{chid}:read",
                                 f"can4home:group_{group}:node_{node}:channel_{chid}:write")
        
    def removeNode(self, group: int, node: int):
        
        logger.debug(f"Deleting node {group}::{node}")
        
        self.clearNodeData(group, node)
        
        self._redisdb.srem('can4home:groups', str(group))
        self._redisdb.srem(f"can4home:group_{group}:nodes", str(node))
        
    def requestNumberOfChannels(self, group : int, node: int):
        
        logger.debug(f"Requesting number of channels of {group}::{node}")
        
        self.requestChannelValue(group, node, 0)
        
    def requestChannelValue(self, group: int, node: int, channel: int):
        
        logger.debug(f"Requesting channel value {group}::{node}::{channel}")
        
        self.sendPacket(group, node, self.CommandList.DATA, channel, True, [], 1)
        
        
    def requestSerialNumber(self, group : int, node: int):
        
        logger.debug(f"Requesting serial number of {group}::{node}")
        
        self.sendPacket(group, node, self.CommandList.SERIALNO, 0, True, [], 8)
    
    def requestChannelDescription(self, group : int, node: int, channel: int):
        
        logger.debug(f"Requesting channel description of {group}::{node}::{channel}")
        
        nbchan = self._redisdb.get(f"can4home:group_{group}:node_{node}:nb_channels")
        if nbchan is None:
            self.requestNumberOfChannels(group, node)
            
        self.sendPacket(group, node, self.CommandList.CHAN_DESCR, channel, True, [], 1)
                     
        
    def _periodicCheckTask(self, deadtime : int):
        
        while self._continue:
            self.periodicCheck()
            
            for i in range(deadtime):
                if not self._continue:
                    break
                
                gevent.sleep(1)
        
    def periodicCheck(self):
        """
        Check periodically the missing parameters from the channel map and bus map. Send request packages to fill the gap.
        """
        
        logger.debug("Performing a periodic check")
        
        groups = self._redisdb.smembers('can4home:groups')
        
#        logger.debug(groups)
        
        for g in groups:
            g = int(g.decode())

            nodes  = self._redisdb.smembers(f"can4home:group_{g}:nodes")
            
#            logger.debug(nodes)
            
            for n in nodes:
                n = int(n.decode())

                serial = self._redisdb.get(f"can4home:group_{g}:node_{n}:serial")
                
                if serial is None:
                    self.requestSerialNumber(g,n)
                
                nbc = self._redisdb.get(f"can4home:group_{g}:node_{n}:nb_channels")
                
                if nbc is None:
                    self.requestNumberOfChannels(g, n)
                else:
                    for c in range(int(nbc.decode())):
                        chd = self._redisdb.mget([f'can4home:group_{g}:node_{n}:channel_{c}:{v}' for v in ('type','read','write')])
                        
#                        logger.debug(chd)
#                        logger.debug([f'can4home:group_{g}:node_{n}:channel_{c}:{v}' for v in ('type','read','write')])

                        if None in chd:
                            self.requestChannelDescription(g, n, c)
                            
                            
    def getChannelType(self, g, n, c):
        v = self._redisdb.get(f'can4home:group_{g}:node_{n}:channel_{c}:type')
        
        if v is None:
            return None
        
        v = v.decode()
        
        return self.DataType(int(v))
    
    @typechecked
    def writeToChannel(self, group: int, node: int, channel: int, val : Sequence[int]):
        self.sendPacket(group, node, self.CommandList.WRITE_DATA, channel, data=val)  

        # gevent.spawn_later(2,self.requestChannelValue, group, node, channel)               
                            
    def getBusDescription(self):
        r = dict()
        
        groups = self._redisdb.smembers('can4home:groups')
        
#        logger.debug(groups)
        
        for g in groups:
            g = int(g.decode())

            nodes  = self._redisdb.smembers(f"can4home:group_{g}:nodes")
            
#            logger.debug(nodes)

            r[g] = dict()
            
            for n in nodes:
                n = int(n.decode())
                
                rr = dict()

                serial = self._redisdb.get(f"can4home:group_{g}:node_{n}:serial")
                nbc = self._redisdb.get(f"can4home:group_{g}:node_{n}:nb_channels")
                
                rr['serial'] = serial.decode() if serial is not None else None
                rr['nbc'] = int(nbc.decode()) if nbc is not None else None
                
                rr['channels'] = dict()
                
                if nbc is not None:
                    for c in range(int(nbc.decode())):
                        
                        rr['channels'][c] = dict()
                        
                        fields = ('read','write')
                        for f in fields: 
                            v = self._redisdb.get(f'can4home:group_{g}:node_{n}:channel_{c}:{f}')
                            rr['channels'][c][f] = None
                            
                            if v is not None:
                                v = v.decode()
                                if v == 'true':
                                    rr['channels'][c][f] = True
                                elif v == 'false':
                                    rr['channels'][c][f] = False
                                    
                        
                        v = self._redisdb.get(f'can4home:group_{g}:node_{n}:channel_{c}:type')
                        rr['channels'][c]['type'] = None
                        
                        if v is not None:
                            v = v.decode()
                            rr['channels'][c]['type'] = self.DataType(int(v))
                            
                r[g][n] = rr
        
        return r
                            
                            
